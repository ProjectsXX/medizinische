package com.example.myapplication.ui.Erinnerung;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.ui.blutzucker.BlutzuckerViewModel;

public class ErinnerungFragment extends Fragment {
    private ErinnerungViewModel erinnerungViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        erinnerungViewModel =
                ViewModelProviders.of(this).get(ErinnerungViewModel.class);

        View root = inflater.inflate(R.layout.fragment_erinnerung, container, false);
        final TextView textView = root.findViewById(R.id.text_erinnerung);
        erinnerungViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}
