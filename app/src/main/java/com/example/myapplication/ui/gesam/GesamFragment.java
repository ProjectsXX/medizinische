package com.example.myapplication.ui.gesam;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myapplication.R;


public class GesamFragment extends Fragment {
    private GesamViewModel gesamViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        gesamViewModel =
                ViewModelProviders.of(this).get(GesamViewModel.class);
        View root = inflater.inflate(R.layout.fragment_gesam, container, false);
        final TextView textView = root.findViewById(R.id.text_gesam);
        gesamViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}
