package com.example.myapplication.ui.temperatur;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;

import com.example.myapplication.R;

public class TemperaturFragment extends Fragment {

    private TemperaturViewModel temperaturViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        temperaturViewModel =
                ViewModelProviders.of(this).get(TemperaturViewModel.class);
        View root = inflater.inflate(R.layout.fragment_temperatur, container, false);
        final TextView textView = root.findViewById(R.id.text_temperatur);
        temperaturViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}
